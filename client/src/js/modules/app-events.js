// $(document).ready(function(){
  
  // App events list
  
  $(document).on('click', '.jsShowMenu', function(e) {
    $(document).find('.main-page').addClass('slide-out-blurred-right');
    $(document).find('.main-page').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
      $(this).addClass('hidden').removeClass('active-page');
    });
    $(document).find('.menu-page').removeClass('hidden');
    
  });

  $(document).on('click', '#add-espresso', function(e) {
    var ordercount = $(document).find('.order-count')[0];
    $(ordercount).addClass('fade-in').removeClass('hidden');
    var cart = $(document).find('.cart-info')[0];
    $(this).fadeOut();
    $(this).siblings('.count').addClass('fade-in').removeClass('hidden');
    $(this).siblings('.selected').addClass('fade-in').removeClass('hidden');
    $(cart).addClass('heartbeat');
  });

  $(document).on('change', '#filter', function(e) {
    $('.menu-list').scrollTop(0);
    var id = $(this).val();
    if (id == 1) {
      const brakfastlist = `
      <div class="dish-card">
        <img src="../dist/img/croisants-menu-item.png" alt="Late">
        <span class="add" id="add-croisants"><img src="../dist/img/add.svg" alt=""></span>
      </div>
      <div class="dish-card" id="benedict">
        <img src="../dist/img/benedict-menu-item.png" alt="Late">
        <span class="add" id="add-croisants"><img src="../dist/img/add.svg" alt=""></span>
        <span class="count hidden"><img src="../dist/img/active-count.svg" alt=""></span>
        <span class="selected hidden"></span>
      </div>
      <div class="dish-card" id="esperesso">
        <img src="../dist/img/espresso-menu-item.png" alt="Espresso">
        <span class="count"><img src="../dist/img/active-count.svg" alt=""></span>
        <span class="selected"></span>
      </div>
      <div class="dish-card mb-20">
        <img src="../dist/img/late-menu-item.png" alt="Late">
        <span class="add" id="add-croisants"><img src="../dist/img/add.svg" alt=""></span>
      </div>
      `
      $('.menu-list').empty().addClass('hidden').append(brakfastlist);
      $('.menu-list').addClass('fade-in').removeClass('hidden');

    }
  });

  $(document).on('click','.checkout', function () {
    $('.detailed-view').removeClass('slide-in-blurred-left').addClass('puff-out-center');
    setTimeout(function() {
      $('.detailed-view').addClass('hidden')
    }, 2000)
    $('.order-count').text('2')
    $('#benedict').find('.add').addClass('hidden')
    $('#benedict').find('.count').fadeIn()
    $('#benedict').find('.selected').fadeIn()
  });

// App events list

$(document).on('click', '.jsShowMenu', function (e) {
  $(document).find('.menu-page').removeClass('hidden');
  $(document).find('.main-page').addClass('hidden', 1000);
});

$('.select-filter .select').change(function (e) {
  $('.select-filter .filtet-text').text($(this).find("option:selected").text())
});

$('.select-sort .select').change(function (e) {
  $('.select-sort .filtet-text').text($(this).find("option:selected").text())
});


$(document).ready(function() {

  $(document).on('click','#benedict', function () { 
    $('.detailed-view').removeClass('hidden');
    $(document).find('.detailed-view').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
      $('.menu-page').addClass('hidden');
    });
  })

});
$('.cart-info').click(function() {
  $('.order-wrap').removeClass('hidden')
})
$('.order-wrap').find('.submit').click(function() {
  $('.main-page').removeClass('hidden slide-in-fwd-center slide-out-blurred-right').css({
    'z-index': '8'
  }).addClass('slide-in-blurred-left')
  $('.main-page').find('.receipt-main').removeClass('hidden')
})
$('.main-page').find('.receipt-main').click(function() {
  $('.receipt').removeClass('hidden').css({
    'z-index': 9
  })
})
$('.receipt-btn').click(function() {
  $('.payment').removeClass('hidden')
})