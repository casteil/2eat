// Smart Search Declarating
window.mainAppModule = (function() {

    function initApp() {
        $(document).find('.main-page').addClass('active-page');
        
    }

    function initMenu() {
        $(document).find('.menu-page').addClass('active-page');
    }

    return {
        oninitApp: function() {
            return initApp();
        },
        oninitMenu: function() {
            return initMenu();
        }
    }

})();


$(document).ready(function() {
    mainAppModule.oninitApp();
});