// Smart Search Declarating
window.httpRequestsService = (function() {

    function getRequest(hash, callback) {
        $.ajax({
            url: `${url}/rest/${hash}`,
            type: "GET",
            processData: false,
            contentType: 'application/json; charset=utf-8',
            success: callback
        });
    }

    return {
        get: function(hash, callback) {
            return getRequest(hash, callback);
        }
    }

})();
