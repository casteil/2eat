const express = require('express')
const app = express()

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});  

app.get('/rest/:id', function (req, res) {
    console.log(req);
    var json = {name:'Sonata', location:'Lviv'};
    res.send(JSON.stringify(json));
})

app.listen(8000, () => console.log('2EAT server listening on 8000 port'))